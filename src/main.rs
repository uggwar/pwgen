extern crate rand;

use rand::distributions::{Distribution, Uniform};

fn main() {
    let words = include_str!("../resources/british-english-small").lines();
    let filtered: Vec<&str> = words
        .filter(|w| w.len() > 4 && w.len() < 13 && !w.contains("'"))
        .collect();
    let between = Uniform::from(0..filtered.len() - 1);
    let mut rng = rand::thread_rng();
    let w1 = filtered.get(between.sample(&mut rng)).unwrap();
    let w2 = filtered.get(between.sample(&mut rng)).unwrap();
    let w3 = filtered.get(between.sample(&mut rng)).unwrap();
    let w4 = filtered.get(between.sample(&mut rng)).unwrap();

    println!("{}.{}.{}.{}", w1, w2, w3, w4);
}
